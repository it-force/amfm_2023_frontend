import React from "react";
import { FaHeart } from "react-icons/fa";
import "./Footer.css"; // Import the CSS file for styling

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container">
        <p className="mb-0">
          © 2023 FUKI. Made with <FaHeart /> by IT Force. All rights reserved.
        </p>
      </div>
    </footer>
  );
};

export default Footer;
