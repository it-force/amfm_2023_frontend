import React, { useState } from "react";

import useForm from "../UseForm/useForm";
import "./AddCollectionModal.css";

import axios from "axios";

function AddCollectionModal({ setOpenModal }) {
  const { values, handleChange, handleSubmit } = useForm(createCollection);
  const [collection, setCollection] = useState();

  async function createCollection() {
    try {
      const addCollection = { ...values };
      let response = await axios.post(
        `http://127.0.0.1:8000/collection/`,
        addCollection
      );
      setCollection(response.data);

      window.location.reload(false);
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div className="modalBackground">
      <div className="modalContainer">
        <div className="titleCloseBtn">
          <button
            onClick={() => {
              setOpenModal(false);
            }}
          >
            X
          </button>
        </div>
        <div className="title">
          <h1>Add Collection</h1>
        </div>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            name="title"
            onChange={handleChange}
            value={collection}
            required={true}
            placeholder="Enter collection name"
          />
        </form>
        <div className="footer">
          <button
            onClick={() => {
              setOpenModal(false);
            }}
            id="cancelBtn"
          >
            Cancel
          </button>
          <button onClick={handleSubmit}>Add</button>
        </div>
      </div>
    </div>
  );
}

export default AddCollectionModal;
