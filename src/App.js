import React, { useState, useEffect } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import CreateCardForm from "./components/CreateCardFrom/createCardForm";
import CollectionsList from "./components/CollectionsList/collectionsList";
import CardsList from "./components/CardsList/cardsList";
import AddCollectionModal from "./components/AddCollectionModal/AddCollection";
import Footer from "./components/Footer/Footer";

function App() {
  const [collections, setCollections] = useState([]);
  const [cards, setCards] = useState([]);
  const [collectionIsSelected, setCollectionIsSelected] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    getAllCollections();
    // getAllCards(collections.id);
  }, []);

  let getAllCollections = async () => {
    try {
      let response = await axios.get("http://localhost:8000/collection");
      console.log(response.data); // test
      setCollections(response.data);
    } catch (err) {
      console.log(err);
    }
  };

  let getAllCards = async (id) => {
    try {
      let response = await axios.get(
        `http://localhost:8000/collection/card/${id}`
      );
      console.log(response.data); // test
      setCards(response.data);
      setCollectionIsSelected(id);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <React.Fragment>
      <div className="app">
        <h1 className="flashcards-header">Fuki Flashcards</h1>
        <div className="App">
          <button
            className="openModalBtn"
            onClick={() => {
              setModalOpen(true);
            }}
          >
            Add Collection
          </button>

          {modalOpen && <AddCollectionModal setOpenModal={setModalOpen} />}
        </div>
      </div>
      <CollectionsList
        collections={collections}
        cards={cards}
        getAllCards={getAllCards}
        collectionIsSelected={collectionIsSelected}
      />
      <CardsList
        collections={collections}
        cards={cards}
        getAllCollections={getAllCollections}
        getAllCards={getAllCards}
        collectionIsSelected={collectionIsSelected}
      />
      <div>
        {collectionIsSelected && (
          <CreateCardForm
            collections={collections}
            cards={cards}
            collectionIsSelected={collectionIsSelected}
          />
        )}
      </div>
      <div>
        {collectionIsSelected == null && (
          <h2 className="choose-collection">
            Please choose a collection of Flashcards
          </h2>
        )}
      </div>

      <Footer />
    </React.Fragment>
  );
}

export default App;
